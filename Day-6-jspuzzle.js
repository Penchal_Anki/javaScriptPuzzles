
/**
 * This this flow is used to check the given data is array or not
 * @flow
 */
//decleare the  student object
const student={
  name:"Penchal",
  class:"5",
  section:"a"
}
//Object.keys used to display the object key value and object.values displays the values of an object
const studentObjectkeyvalue = Object.keys(student);
const studentObjectValue=Object.values(student);
//the above values and keys displays in array
//the loop is shows the plain values
for(var i=0;i<studentObjectkeyvalue.length;i++){
console.log(studentObjectkeyvalue[i]);
}
console.log(studentObjectkeyvalue);
console.log(studentObjectValue);

/*
Output:
name
class
section
["name","class","section"]
["apn","5","a"]
*/